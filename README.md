Изменения по сравнению с версией находящейся в GitHub: https://github.com/NikSubbotin/TestProject/commit/5738efc1f3b42ecc927827501ef5d6b65ac956c4

Используются потоки (Streams) для обработки данных;
Добавлен интерфейс OperationOnNumbers;
Убран вспомогательный класс ImportDataFromFile, импорт данных осуществляется в main методе