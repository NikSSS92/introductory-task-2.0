import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        SelectCommand selectCommand = new SelectCommand();

        boolean selectSource = false;

        int[] data1 = Files.lines(Paths.get("resources/TestData")).mapToInt(Integer::parseInt).toArray(); //Import data from file
        int[] data2 = new int[100];
        for(int i = 0; i < data2.length; i++){
            data2[i] = (int) (Math.random()*(100));
        }

        int[] dataIn;

        if(selectSource){
            dataIn = data1;
        } else {
            dataIn = data2;
        }

        System.out.println("Данные получены. Введите код команды. \n" +
                "Для получения списка доступных команд введите help \n" +
                "Для выхода из программы введите end.");

        Scanner scanner = new Scanner(System.in);
        OperationOnNumbers operationOnNumbers = new ImplementationСommands();

        String codeCommand;

        do {
            codeCommand = scanner.nextLine();
            selectCommand.selectCommand(codeCommand, dataIn, operationOnNumbers);
        } while (!codeCommand.equals("end"));

        scanner.close();

    }
}
